import array
import random

import numpy
import numpy as np
from matplotlib import pyplot as plt

SIMULATIONS_COUNT = 50
ARRAY_SIZE = 100


def create_array(numbers_count: int) -> numpy.array:
    numbers = np.array([0])

    for i in range(numbers_count):
        numbers = numpy.append(numbers, numbers[i] + random.uniform(-1, 1))
    return numbers


def plot_array(arr: array):
    plt.title('Result')
    plt.xlabel('Index')
    plt.ylabel('Value')
    plt.plot(arr)
    plt.show()


def simulate(simulations_count: int):
    max_elements = []
    exceeded_thirty_counter = 0

    for i in range(simulations_count):
        arr = create_array(ARRAY_SIZE)
        plot_array(arr)
        min_element = numpy.amin(arr)
        max_element = numpy.amax(arr)
        max_elements.append(max_element)
        if max_element > 30:
            exceeded_thirty_counter += 1

    plt.title("Result")
    plt.xlabel("Simulation")
    plt.ylabel("Highest value")
    plt.ylim(ymin=0)
    plt.ylim(ymax=40)
    plt.plot(max_elements)
    plt.show()

    print(f'Exceeded the value of thirthy {exceeded_thirty_counter} times.')


simulate(SIMULATIONS_COUNT)
